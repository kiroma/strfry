#include "curser.hpp"
#include "runner.hpp"

int main()
{
	initialize_curses();
	run();
	return 0;
}
