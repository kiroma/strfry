#include "runner.hpp"
#include "curser.hpp"
#include "strings.hpp"
#include <thread>
void run()
{
	std::atomic<bool> predicate;
	std::string str;
	std::mutex mtx_random;
	std::condition_variable cv;
	std::unique_lock<std::mutex> lk(mtx_random);
	lk.unlock();
	std::thread listener(listen, std::ref(str), std::ref(mtx_random));
	listener.detach();
	std::thread randomizer(strfry, std::ref(str), std::ref(cv), std::ref(mtx_random), std::ref(predicate));
	randomizer.detach();
	auto a = std::chrono::high_resolution_clock::now(),
		b = std::chrono::high_resolution_clock::now();
	while(true)
	{
		a = std::chrono::high_resolution_clock::now();
		lk.lock();
		mvprintw(0, 0, str.c_str());
		lk.unlock();
		predicate = true;
		cv.notify_all();
		refresh();
		b = std::chrono::high_resolution_clock::now();
		std::this_thread::sleep_for(std::chrono::nanoseconds(std::chrono::seconds(1)/60) - (a-b));
	}
}
