#include "curser.hpp"

void initialize_curses()
{
	initscr();
	cbreak();
	curs_set(0);
	keypad(stdscr, true);
	noecho();
	nodelay(stdscr, true);
}
