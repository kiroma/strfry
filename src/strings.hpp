#ifndef STRINGS_HPP_INCLUDED
#define STRINGS_HPP_INCLUDED
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <string>

void listen(std::string &str, std::mutex &mtx);

void strfry(std::string &str, std::condition_variable &cv, std::mutex &mtx, std::atomic<bool> &predicate);

#endif
