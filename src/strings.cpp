#include "strings.hpp"
#include <algorithm>

void listen(std::string &str, std::mutex &mtx)
{
	while(true)
	{
		char ch = getchar();
		mtx.lock();
		str += ch;
		mtx.unlock();
	}
}

void strfry(std::string &str, std::condition_variable &cv, std::mutex &mtx, std::atomic<bool> &predicate)
{
	while(true)
	{
		std::unique_lock<std::mutex> lk(mtx);
		cv.wait(lk, [&predicate]()->bool{return predicate;});
		predicate = false;
		std::random_shuffle(str.begin(), str.end());
	}
}
