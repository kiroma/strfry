# strfry

This is a repository for a small project that shows off several things in C++:
- Thread creation with passing by reference
- Using condition_variable to synchronize threads
- Securing condition_variable with a predicate
- Lambda functions
- Creation of an ncurses context
- Running an ncurses window
- FPS limiting

The project takes input from players and displays it on the screen, while constantly randomizing position of the characters.
